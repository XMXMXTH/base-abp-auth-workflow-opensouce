import pagedTable from '../common/pagedTable'
import { getAuditLogs, getAuditLogsToExcel } from '@/api/auditlogs'

const auditlogs = {
  namespaced: true,
  state: {
    ...pagedTable.state,
    pageSize: 50,
    auditlogstoExcelLoading: false,
    top3Notifications: {}
  },
  mutations: {
    ...pagedTable.mutations
  },
  actions: {
    ...pagedTable.actions,

    // 覆盖默认实现
    getAll({ state }, payload) {
      return new Promise((resolve, reject) => {
        state.loading = true
        getAuditLogs(payload.data)
          .then(response => {
            state.list = []
            state.list.push(...response.data.result.items)
            state.totalCount = response.data.result.totalCount
            resolve(response.data.result)
          })
          .catch(error => {
            reject(error)
          })
          .finally(() => {
            state.loading = false
          })
      })
    },

    getAuditLogsToExcel({ state }, payload) {
      state.auditlogstoExcelLoading = true
      return new Promise((resolve, reject) => {
        getAuditLogsToExcel(payload.data)
          .then(response => {
            resolve(response.data.result)
          })
          .catch(error => {
            reject(error)
          })
          .finally(() => {
            state.auditlogstoExcelLoading = false
          })
      })
    }
  }
}

export default auditlogs
