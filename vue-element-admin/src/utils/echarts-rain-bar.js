import echarts from 'echarts'
// import rainChartData from './mock/rain-charts'
import moment from 'moment'

const rainBar = {
  _rainChart: null,

  _rainData: [],

  _barOptions: {
    animation: false,
    color: ['#0e7bd8'],
    tooltip: {},
    grid: {
      show: true,
      top: '39px',
      left: '30px',
      right: '10px',
      bottom: '33px',
      backgroundColor: '#f5f5f5',
      borderColor: '#cccccc'
    },
    xAxis: [
      {
        type: 'category',
        data: [],
        axisLabel: {
          formatter: value => {
            return moment(value)
              .format('HH时 MM-DD')
              .replace(' ', '\n')
          }
        }
      }
    ],
    yAxis: [
      {
        type: 'value',
        name: '雨量(mm)'
      }
    ],
    series: [
      {
        name: '雨量(mm)',
        type: 'bar',
        barWidth: '60%',
        data: [],
        tooltip: {
          trigger: 'item',
          formatter: params => {
            const data = params.data
            const unit = '(mm)'
            const tooltip = `<div class="tooltip-container"><span class="tooltip-title"><b>时间:</b>${moment(
              data.time
            ).format(
              'YYYY-MM-DD HH:mm'
            )}</span><br/><span class="tooltip-title"><b>雨量:</b>${
              data.value ? data.value.toFixed(1) : 0
            }${unit}</span>`
            return tooltip
          }
        }
      }
    ]
  },

  _fetchData(stcd, tm) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        // resolve(rainChartData)
      }, 300)
    })
  },

  init(divId) {
    var barOptions = this._barOptions
    var rainChart = echarts.init(document.getElementById(divId))
    rainChart.clear()
    rainChart.setOption(barOptions)
    this._rainChart = rainChart
  },

  showLoading() {
    var rainChart = this._rainChart
    rainChart.showLoading({
      text: '加载中...',
      color: '#3398DB'
    })
  },

  hideLoading() {
    var rainChart = this._rainChart
    rainChart.hideLoading()
  },

  update(fetchData) {
    var barOptions = this._barOptions
    var rainChart = this._rainChart
    this._rainData = []
    // barOptions.xAxis[0].data = []
    // barOptions.series[0].data = []
    rainChart.clear()

    this._rainData = fetchData

    barOptions.xAxis[0].data = this._rainData.data.map(d => d.time)
    barOptions.series[0].data = this._rainData.data.map(d => {
      return {
        time: d.time,
        value: d.val
      }
    })
    rainChart.setOption(barOptions)
  },

  async Render(divId, stcd, tm) {
    var barOptions = this._barOptions
    this._rainData = []
    // barOptions.xAxis[0].data = []
    // barOptions.series[0].data = []
    var rainChart = echarts.init(document.getElementById(divId))
    rainChart.clear()
    rainChart.setOption(barOptions)

    rainChart.showLoading({
      text: '加载中...',
      color: '#3398DB'
    })
    this._rainData = await this._fetchData(stcd, tm)
    rainChart.hideLoading()

    barOptions.xAxis[0].data = this._rainData.data.map(d => d.time)
    barOptions.series[0].data = this._rainData.data.map(d => {
      return {
        time: d.time,
        value: d.val
      }
    })
    rainChart.setOption(barOptions)
  }
}

export default rainBar
