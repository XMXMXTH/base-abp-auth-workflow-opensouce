import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/api/TokenAuth/Authenticate',
    method: 'post',
    data
  })
}

export function externalLogin(data) {
  return request({
    url: '/api/TokenAuth/ExternalAuthenticate',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}

export function get(id) {
  return request({
    url: '/api/services/app/User/Get',
    method: 'get',
    params: { id: id }
  })
}

export function getAll(data) {
  return request({
    url: '/api/services/app/User/GetAll',
    method: 'get',
    params: data
  })
}

export function getUsers(data) {
  return request({
    url: '/api/services/app/User/GetUsers',
    method: 'get',
    params: data
  })
}

export function deleteById(id) {
  return request({
    url: '/api/services/app/User/Delete',
    method: 'delete',
    params: { id: id }
  })
}

export function create(data) {
  return request({
    url: '/api/services/app/User/Create',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/api/services/app/User/Update',
    method: 'put',
    data
  })
}

export function getRoles() {
  return request({
    url: '/api/services/app/User/GetRoles',
    method: 'get'
  })
}

export function changeLanguage(data) {
  return request({
    url: '/api/services/app/profile/ChangeLanguage',
    method: 'post',
    data
  })
}

export function changePassword(data) {
  return request({
    url: '/api/services/app/profile/ChangePassword',
    method: 'post',
    data
  })
}
