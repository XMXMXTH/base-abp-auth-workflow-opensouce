import request from '@/utils/request'

export function getCurrentUserProfileForEdit() {
  return request({
    url: '/api/services/app/profile/GetCurrentUserProfileForEdit',
    method: 'get',
    params: {}
  })
}

export function updateCurrentUserProfile(data) {
  return request({
    url: '/api/services/app/profile/UpdateCurrentUserProfile',
    method: 'put',
    data
  })
}

export function changeLanguage(data) {
  return request({
    url: '/api/services/app/profile/ChangeLanguage',
    method: 'post',
    data
  })
}

export function changePassword(data) {
  return request({
    url: '/api/services/app/profile/ChangePassword',
    method: 'post',
    data
  })
}

export function getProfilePicture() {
  return request({
    url: '/api/services/app/profile/GetProfilePicture',
    method: 'get'
  })
}

export function updateProfileRawPicture(data) {
  return request({
    url: '/api/services/app/profile/UpdateProfileRawPicture',
    method: 'put',
    data
  })
}

export function getProfilePictureById(profilePictureId) {
  return request({
    url: '/api/services/app/profile/GetProfilePictureById',
    method: 'get',
    params: { profilePictureId }
  })
}
