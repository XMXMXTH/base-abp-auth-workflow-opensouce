import request from '@/utils/request'

export function getDeviceTypes(data) {
  return request({
    baseURL: process.env.VUE_APP_MAP_BASE_API,
    url: '/api/services/app/DeviceType/GetAll',
    method: 'get',
    params: data
  })
}

export function getCitys() {
  return request({
    baseURL: process.env.VUE_APP_MAP_BASE_API,
    url: '/api/services/app/DeviceBindInfo/GetCitys',
    method: 'get',
    params: {}
  })
}

export function getLastData(payload) {
  return request({
    baseURL: process.env.VUE_APP_MAP_BASE_API,
    url: '/api/services/app/LastData/GetAll',
    method: 'get',
    params: payload
  })
}

export function getOriginalDatas(payload) {
  return request({
    baseURL: process.env.VUE_APP_MAP_BASE_API,
    url: '/api/services/app/OriginalData/GetOriginalDatas',
    method: 'get',
    params: payload
  })
}

export function getOriginalMsgGPRSById(payload) {
  return request({
    baseURL: process.env.VUE_APP_MAP_BASE_API,
    url: '/api/services/app/OriginalData/GetOriginalMsgGPRSById',
    method: 'get',
    params: payload
  })
}

export function getOnlyLastDatas(payload) {
  return request({
    baseURL: process.env.VUE_APP_MAP_BASE_API,
    url: '/api/services/app/LastData/GetLastDatas',
    method: 'get',
    params: payload
  })
}

export function getWaterChartData(payload) {
  return request({
    baseURL: process.env.VUE_APP_MAP_BASE_API,
    url: '/api/services/app/OriginalData/GetWaterChartDatas',
    method: 'get',
    params: payload
  })
}

export function getRainChartData(payload) {
  return request({
    baseURL: process.env.VUE_APP_MAP_BASE_API,
    url: '/api/services/app/OriginalData/GetRainChartDatas',
    method: 'get',
    params: payload
  })
}
